import React, { Component } from "react";
import { connect } from "react-redux";
import Cart from "./Cart";
import ItemShoe from "./ItemShoe";

class ProductList extends Component {
  render() {
    return (
      <div className="row">
        {/* danh sach giay */}
        <div className="row col-6" id="productList">
          {this.props.productsData.map((item, index) => {
            return <ItemShoe shoe={item} key={index} />;
          })}
        </div>
        {/* danh sach cart */}
        <div className="col-6" id="cart" style={{position:"fixed", right:"0"}} >
          {this.props.cart.length > 0 && (
            <Cart 
            />
          )}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    productsData: state.shoesShopReducer.productsData,
    cart: state.shoesShopReducer.cart,
  };
};
export default connect(mapStateToProps)(ProductList);
