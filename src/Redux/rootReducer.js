import { combineReducers } from "redux";
import { shoesShopReducer } from "./ShoesShopReducer";

export const rootReducer = combineReducers({
  shoesShopReducer,
});
